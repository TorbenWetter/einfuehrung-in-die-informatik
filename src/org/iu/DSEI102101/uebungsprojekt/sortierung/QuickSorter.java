package org.iu.DSEI102101.uebungsprojekt.sortierung;

import java.util.LinkedList;
import java.util.List;

public class QuickSorter {

    public static int[] sort(int[] array) {
        if (array.length <= 1) {
            return array;
        }

        int pivot = array[0];
        List<Integer> smallerList = new LinkedList<>();
        List<Integer> largerList = new LinkedList<>();
        for (int i = 1; i < array.length; i++) {
            if (array[i] <= pivot) {
                smallerList.add(array[i]);
            } else {
                largerList.add(array[i]);
            }
        }
        int[] smaller = new int[smallerList.size()];
        int smallerIndex = 0;
        for (Integer small : smallerList) {
            smaller[smallerIndex++] = small;
        }
        int[] larger = new int[largerList.size()];
        int largerIndex = 0;
        for (Integer large : largerList) {
            larger[largerIndex++] = large;
        }

        int[] sorted = new int[smallerList.size() + 1 + largerList.size()];
        int index = 0;
        for (int small : sort(smaller)) {
            sorted[index++] = small;
        }
        sorted[index++] = pivot;
        for (int large : sort(larger)) {
            sorted[index++] = large;
        }
        return sorted;
    }
}
