package org.iu.DSEI102101.uebungsprojekt.sortierung;

public class BubbleSorter {

    public static int[] sort(int[] array) {
        int[] cloned = array.clone();
        bubbleSort(cloned);
        return cloned;
    }

    private static void bubbleSort(int[] array) {
        int iterations = 0;
        boolean swapped;
        do {
            swapped = false;
            for (int i = 0; i < array.length - 1 - iterations; i++) {
                if (array[i] > array[i + 1]) {
                    int temp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = temp;
                    swapped = true;
                }
            }
            iterations++;
        } while (swapped);
    }
}
