package org.iu.DSEI102101.uebungsprojekt.chatprogram;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ConnectException;
import java.net.Socket;
import java.util.Scanner;

/**
 * Hinweise zu untenstehendem Code: Untenstehendes Beispiel ist Teil eins sehr
 * einfachen Projektes, welches einen Anfrage-/Antwort-Server realisiert. Um den
 * Blick auf die Standard-Funktionen der Socket-Programmierung zu lenken, wurden
 * häufig verwendete Mechanismen, z. B. zum Umgang mit BufferedReader etc. außer
 * Acht gelassen. Für professionelle Programmierung sollten derartige
 * Mechanismen natürlich berücksichtigt werden. Bitte beachten Sie insofern,
 * dass es sich hierbei um ein unvollständiges Lehrbeispiel handelt. Bitte lesen
 * Sie die Ausführungen bei [Cz21] bzw. sonstiger Literatur nach.
 * <p>
 * Eine gute Erklärung für die Programmierung mit Sockets findet sich bei
 * [Cz21]. In untenstehendem Code werden dieselben Java-Standardbefehle
 * verwendet, die bei [Cz21] erklärt sind.
 * <p>
 * [Cz21] Czeschla, Jörg (o. J.): "Wie schreibe ich einen Server und einen
 * Client, die über Sockets miteinander kommunizieren?". aus den
 * Internetinformationen von javabeginners unter der URL:
 * https://javabeginners.de/Netzwerk/Socketverbindung.php [Stand: 20.12.2021,
 * copyright javabeginners 2021].
 */
public class SimpleClient {

    public static void main(String[] args) {
        try {
            // Create client socket (wait for server to be available)
            Socket socket = null;
            while (socket == null) {
                try {
                    socket = new Socket("localhost", 8634);
                } catch (ConnectException ex) {
                    Thread.sleep(100);
                }
            }

            // Initialize scanner and output stream
            Scanner scanner = new Scanner(System.in);
            PrintStream out = new PrintStream(socket.getOutputStream());

            // Initialize input stream and reader
            InputStream in = socket.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));

            while (true) {
                // Receive message from server
                String serverMessage = reader.readLine();
                System.out.println("Nachricht: " + serverMessage);

                // Send message to server
                System.out.print("Antwort: ");
                String clientMessage = scanner.nextLine();
                out.println(clientMessage);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
