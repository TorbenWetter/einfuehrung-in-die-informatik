package org.iu.DSEI102101.uebungsprojekt.chatprogram;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

/**
 * Hinweise zu untenstehendem Code: Untenstehendes Beispiel ist Teil eins sehr
 * einfachen Projektes, welches einen Anfrage-/Antwort-Server realisiert. Um den
 * Blick auf die Standard-Funktionen der Socket-Programmierung zu lenken, wurden
 * häufig verwendete Mechanismen, z. B. zum Umgang mit BufferedReader etc. außer
 * Acht gelassen. Für professionelle Programmierung sollten derartige
 * Mechanismen natürlich berücksichtigt werden. Bitte beachten Sie insofern,
 * dass es sich hierbei um ein unvollständiges Lehrbeispiel handelt. Bitte lesen
 * Sie die Ausführungen bei [Cz21] bzw. sonstiger Literatur nach.
 * <p>
 * Eine gute Erklärung für die Programmierung mit Sockets findet sich bei
 * [Cz21]. In untenstehendem Code werden dieselben Java-Standardbefehle
 * verwendet, die bei [Cz21] erklärt sind.
 * <p>
 * [Cz21] Czeschla, Jörg (o. J.): "Wie schreibe ich einen Server und einen
 * Client, die über Sockets miteinander kommunizieren?". aus den
 * Internetinformationen von javabeginners unter der URL:
 * https://javabeginners.de/Netzwerk/Socketverbindung.php [Stand: 20.12.2021,
 * copyright javabeginners 2021].
 */
public class SimpleServer {

    public static void main(String[] args) {
        try {
            // Create server socket
            ServerSocket serverSocket = new ServerSocket(8634);

            // Accept a client connection
            Socket socket = serverSocket.accept();

            // Initialize scanner and output stream
            Scanner scanner = new Scanner(System.in);
            PrintStream out = new PrintStream(socket.getOutputStream());

            // Initialize input stream and reader
            InputStream in = socket.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));

            while (true) {
                // Send message to client
                System.out.print("Nachricht: ");
                String serverMessage = scanner.nextLine();
                out.println(serverMessage);

                // Receive message from client
                String clientMessage = reader.readLine();
                System.out.println("Antwort: " + clientMessage);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
