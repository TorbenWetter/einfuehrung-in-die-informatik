package org.iu.DSEI102101.uebungsprojekt;

import org.iu.DSEI102101.uebungsprojekt.sortierung.BubbleSorter;
import org.iu.DSEI102101.uebungsprojekt.sortierung.QuickSorter;

public class Sortierung {

    public static void main(String[] args) {
        int iterations = 100, arraySize = 10000, minValue = 0, maxValue = 100;
        long totalBubbleSortTime = 0, totalQuickSortTime = 0;
        for (int iteration = 0; iteration < iterations; iteration++) {
            int[] array = new int[arraySize];
            for (int i = 0; i < arraySize; i++) {
                array[i] = (int) (Math.random() * (maxValue - minValue + 1) + minValue);
            }

            long bubbleSortStartTime = System.currentTimeMillis();
            BubbleSorter.sort(array);
            totalBubbleSortTime += System.currentTimeMillis() - bubbleSortStartTime;

            long quickSortStartTime = System.currentTimeMillis();
            QuickSorter.sort(array);
            totalQuickSortTime += System.currentTimeMillis() - quickSortStartTime;
        }
        System.out.println("Durchschnittliche Zeit für das Sortieren von " + arraySize + " Elementen");
        System.out.println("BubbleSort: " + totalBubbleSortTime / iterations + " ms");
        System.out.println("QuickSort: " + totalQuickSortTime / iterations + " ms");
    }
}
